// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_PDELAB_BACKEND_TAGS_HH
#warning The file dune/pdelab/backend/tags.hh is deprecated. Please use dune/pdelab/backend/common/tags.hh instead.
#include <dune/pdelab/backend/common/tags.hh>
#endif // DUNE_PDELAB_BACKEND_TAGS_HH
